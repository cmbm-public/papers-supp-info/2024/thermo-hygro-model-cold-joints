#!/usr/bin/env python3

import numpy as np
import os
import sys
from bamboost import Manager
from itertools import product
import input_template as itp
import config


# folder where primitive data is written before it is imported into bamboost
tmpfolder= '/cluster/scratch/mhlobil/'

# path to simulation data: executable and postprocess scripts
#src_path  = '/cluster/home/dkammer/sources/bamboost-demo/uguca/'
#exe_file  = os.path.join(src_path,'build/simulations/demo_one/demo_one')
#pp_file   = os.path.join(src_path,'simulations/demo_one/postprocess.py')

src_path= '/cluster/home/mhlobil/project_ColdJoints/simulations_v8_SA1D_default/'
exe_file= os.path.join(src_path,'MultiscaleModel_v8_module.py')
pp_file= os.path.join(src_path,'postprocess.py')


################################################################################################################################
#### CREATE DATABASE OF SIMULATIONS
db= Manager(config.datafolder) # location of stored data: best in 'work' folder on Euler

# define parameters
width= [0.2] #np.arange(0.1, 0.401, 0.025) # [0.2] # (0.06-0.4) [m]
height= [1.6, 2.0] #np.arange(0.6, 2.01, 0.1) # [1.0] # (0.6-2) [m]
D= [200] #np.arange(20,201,20) # [200] [mm^2/day] diffusivity coefficient
Tini= [20] #np.arange(10, 50.001, 2.) # [20] (15-35) [degC]; material temperature throughout the cross-section
T_remote= [20] #np.arange(6, 50.001, 2.) # [20] (5-50) [-]; ambient temperature
heatFlowSides= [10] #np.arange(1, 21, 1) # [10] [W/(m^2 K)] coefficient of heat transfer
H_remote= [0.60] #np.arange(0.30, 0.701, 0.025) # [0.60] ambient humidity [-]
humFlowSides= [1.0] #np.arange(0.25, 8.01, 0.25) # [1.0] [mm/day] coefficient of hygric exchange between surface and environment)

# We can use product from itertools to get all combinations from the two arrays
for width, height, D, Tini, T_remote, heatFlowSides, H_remote, humFlowSides in product(width, height, D, Tini, T_remote, heatFlowSides, H_remote, humFlowSides):

    # create dictionary
    params= {'width': width,
              'height': height,
              'D': D,
              'Tini': Tini,
              'T_remote': T_remote,
              'heatFlowSides': heatFlowSides,
              'H_remote': H_remote,
              'humFlowSides': humFlowSides,
              }

    # create simulation entry in database
    sim= db.create_simulation(parameters=params)

################################################################################################################################
#### LOOP THROUGH ALL SIMULATIONS AND EXECUTE:
db= Manager(config.datafolder)
for sim in db.sims(db.df.status=='Initiated', return_writer=True):
#     print(f'Starting simulation {sim.uid}', flush=True)
    
    # write input file
    input_file = os.path.join(sim.path, sim.uid+".in")
    with open(input_file, "w") as inp:
        print(itp.template.format(**sim.parameters), file=inp)

    # write output file into scratch directory    
    out_file= os.path.join(tmpfolder, sim.uid+".out")

    # list of commands to be executed by run
    commands = []

    # copy and include command that will load modules
    module_file = 'modules.lmod.sh'
    sim.copy_file(module_file)
    commands.append('source '+os.path.join(sim.path,module_file))

    # simulation run command
    #commands.append('{{MPI}}{} {} {}'.format(exe_file,input_file,tmpfolder))
    commands.append('python3 {} {} {}'.format(exe_file, input_file, out_file))

    # postprocess command
    commands.append('python3 {} --path {} --uid {} --opath {}'.format(pp_file, config.datafolder, sim.uid, tmpfolder))

    # create execution files
    sim.create_batch_script(commands, euler=True, ntasks=1)

    # save git attributes
    #sim.register_git_attributes('./')      # this simulation repository
    #sim.register_git_attributes(src_path)  # for uguca

    # start job
    sim.submit()
