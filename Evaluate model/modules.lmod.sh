#env2lmod

module purge

module load StdEnv
module load gcc/6.3.0
module load openmpi/3.0.1
module load openblas/0.2.20
module load python/3.7.4
module load boost/1.66.0
module load eigen/3.3.4
module load hdf5/1.10.1
module load metis/5.1.0
module load parmetis/4.0.3
module load petsc/3.9.4
module load scotch/6.0.4
module load suite-sparse/5.1.0
module load trilinos/12.8.1
module load gmp/6.1.2
module load mpfr/4.0.1
module load fenics/2019.1.0

module list