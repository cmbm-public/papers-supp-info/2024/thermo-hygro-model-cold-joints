#!/usr/bin/env python3

import argparse
import os
import sys
from bamboost.simulation_writer import SimulationWriter

def main(path, uid, opath):

    # bamboost: access the simulation
    writer = SimulationWriter(uid, path)
    writer.add_metadata()

    with open(os.path.join(opath, writer.uid+'.out')) as fl:
        t= float(fl.read())

    #writer.add_global_field("timeCJ", t/60.)
    writer.update_parameters({"timeCJ":t/60.})
    writer.finish_sim()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str)
    parser.add_argument('--uid', type=str)
    parser.add_argument('--opath', type=str)
    args = parser.parse_args()

    main(args.path, args.uid, args.opath)
