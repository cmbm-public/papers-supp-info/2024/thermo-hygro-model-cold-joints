# Simulation of cold joint formation in extruded concrete structures

## Motivation & problem description
This repo contains the implementation of a multiphysics model for predicting cold joints in extruded concrete structures. In practice, extruded concrete structures will heat up (as cement dissolves), and their unprotected surface will dry due to heat and humidity exchange with their surrounding environment. Once the surface humidity of the material drops below a threshold value, a dry crust will form and will limit adhesion to a sequentially-applied new concrete layer. The dry crust between two layers of extruded concrete is denoted as a cold joint.

The modeling concept rests on formulating coupled temperature and humidity transport phenomena within a 2D concrete cross-section, representing a one m-long segment of an infinite extruded structure. As a result, the model predicts the time needed to reach a limiting surface humidity on the cross-section's top surface, indicating cold joint formation. 


## Model evaluation

### Requirements
The model uses a Finite Element analysis library implemented in the open-source [FEniCS package](https://fenicsproject.org/) to solve the coupled temperature-humidity transport problem. As such, it is recommended to install a virtual environment with conda and set up
- python3
- [FEniCS 2019.1.0](https://fenicsproject.org/download/archive/)
- [tqdm] (https://github.com/tqdm/tqdm) 
- [bamboost] (https://gitlab.com/cmbm-ethz/bamboost)

Note: FEniCS 2019 is now a legacy version, but the model was not tested in the current FEniCSX implementation. Thus, to run the model, FEniCS 2019.1.0 is strongly recommended.

### Prepare inputs
The input file (*.dat) contains the principal unknown variables that influence the drying rate of concrete in the given conditions. The user can specify the cross-section geometry (width, height), material parameters of concrete (diffusivity coefficient, initial temperature), ambient conditions (temperature, *relative* humidity), and interaction of the structure with the environment (transfer coefficients of heat and humidity). Other physical/numerical parameters are embedded into the main model code but are not meant to be modified by the user.

### Run a simulation
To run the model, simply activate the virtual environment and execute

```
$ python3 MultiscaleModel_v8_module.py inputParameters.dat
```

The calculated time (min) for cold joint formation will be printed in the prompt as result.

Model evaluation can take a while depending on mesh size, time step chosen, and solver tolerances set. None of these are meant to be blindly modified but can be edited in the python code if needed.


## Authors and acknowledgment
We would like to acknowledge the contributions of Luca Michel, Mohit Pundir, and David Kammer, who helped shape the model development, as well as the funding from the Swiss National Science Foundation under grant 200021_200343.
